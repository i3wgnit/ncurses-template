#pragma once
#ifndef VIEW_H
#define VIEW_H

namespace model {
  class State;
} // namespace model

namespace view {
  class View {
  public:
    View() = default;
    virtual ~View() = default;

    void update(const model::State& state) { updatePriv(state); }

  private:
    virtual void updatePriv(const model::State&) = 0;
  };
} // namespace view
#endif // VIEW_H
