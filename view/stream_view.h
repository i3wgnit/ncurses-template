#pragma once
#ifndef VIEW_STREAM_VIEW_H
#define VIEW_STREAM_VIEW_H
#include <cassert>
#include <filesystem>
#include <ostream>

#include "view.h"

namespace view {
  class StreamView final: public View {
  public:
    explicit StreamView(std::ostream* const stream): stream_(stream) {
      assert(stream_);
    }

    std::ostream* stream() { return stream_; }
    const std::ostream* stream() const { return stream_; }
    void stream(std::ostream* const stream) {
      assert(stream);
      stream_ = stream;
    }

  private:
    void updatePriv(const model::State&) override;

    template<typename InputIt>
    void printFilenames(InputIt first, InputIt last);

    static std::string formatDirectory(const std::filesystem::directory_entry&);

    static std::string formatFile(const std::filesystem::directory_entry&);

    static std::string formatEntry(const std::filesystem::directory_entry&);

    std::ostream* stream_;
  };
} // namespace view
#endif // VIEW_STREAM_VIEW_H
