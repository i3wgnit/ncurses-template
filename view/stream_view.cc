#include <algorithm>
#include <cassert>
#include <iterator>
#include <string>

#include "stream_view.h"
#include "state/state_include.h"

namespace fs = std::filesystem;

namespace view {
  void StreamView::updatePriv(const model::State& state) {
    using model::Window;

    assert(stream_);

    MODEL_STATE_SETUP_REF();

    const fs::path& curPath = window->pwd();
    assert(!curPath.empty());

    (*stream_) << '\n' << curPath.string() << "/\n";

    if (window->empty()) {
      (*stream_) << "- empty" << std::endl;
      return;
    }

    assert(window->entryIndex() < window->size());

    const Window::entries_type& directories = window->directories();
    const Window::entries_type& files = window->files();

    std::vector<std::string> filenames(window->size());
    const size_t numDirectories = directories.size();
    const size_t numFiles = files.size();
    size_t fileIndex = window->entryIndex();
    if (window->reversed()) {
      for (size_t i = 0; i < numDirectories; ++i) {
        filenames[i] = formatDirectory(directories[numDirectories - i - 1]);
      }
      for (size_t i = 0; i < numFiles; ++i) {
        filenames[i + numDirectories] = formatFile(files[numFiles - i - 1]);
      }
    } else {
      for (size_t i = 0; i < numDirectories; ++i) {
        filenames[i] = formatDirectory(directories[i]);
      }
      for (size_t i = 0; i < numFiles; ++i) {
        filenames[i + numDirectories] = formatFile(files[i]);
      }
    }

    const auto filenamesBegin = filenames.cbegin();
    const auto currentFilename = std::next(filenamesBegin, fileIndex);
    printFilenames(filenamesBegin, currentFilename);
    (*stream_) << "* " << *currentFilename << '\n';
    printFilenames(std::next(currentFilename), filenames.cend());

    (*stream_) << std::flush;
  }

  template<typename InputIt>
  void StreamView::printFilenames(InputIt first, InputIt last) {
    std::for_each(first, last,
                  [this](const auto& e) {
                    (*stream_) << "  " << e << '\n';
                  });
  }

  std::string StreamView::formatDirectory(const fs::directory_entry& entry) {
    std::string name = entry.path().filename().string();
    name += '/';

    if (entry.is_symlink()) {
      assert(entry.exists());
      name += " -> ";
      const fs::path canonicalFile = fs::canonical(entry.path());
      name += canonicalFile.string();
      name += '/';
    }

    return name;
  }

  std::string StreamView::formatFile(const fs::directory_entry& entry) {
    std::string name = entry.path().filename().string();

    if (entry.is_symlink()) {
      assert(entry.exists());
      name += " -> ";
      const fs::path canonicalFile = fs::canonical(entry.path());
      name += canonicalFile.string();
    }

    return name;
  }

  std::string StreamView::formatEntry(const fs::directory_entry& entry) {
    std::string name = entry.path().filename().string();
    const bool entryIsDirectory = entry.is_directory();
    if (entryIsDirectory) {
      name += '/';
    }

    if (entry.is_symlink()) {
      assert(entry.exists());
      name += " /-> ";
      const fs::path canonicalFile = fs::canonical(entry.path());
      name += canonicalFile.string();
      if (entryIsDirectory) {
        name += '/';
      }
    }

    return name;
  }
} // namespace view
