#include "keyboard_controller.h"
#include "provider/keyboard_provider.h"

#include "command/command.h"
#include "command/command.h"
#include "command/cursor_down.h"
#include "command/cursor_up.h"
#include "command/go_current.h"
#include "command/go_parent.h"
#include "command/quit.h"
#include "command/quit_all.h"

namespace controller {
  KeyboardController::KeyboardController(std::unique_ptr<KeyboardProvider>
                                         provider):
    provider_(std::move(provider)) {
    assert(provider_);
  }

  void KeyboardController::provider(std::unique_ptr<KeyboardProvider>
                                    provider) {
    assert(provider);
    provider_.swap(provider);
  }

  std::unique_ptr<model::Command> KeyboardController::getCommandPriv() {
    assert(provider_);

    KeyboardProvider::Char c;
    while (provider_->getChar(&c)) {
      switch (c.character_) {
      case 'q':
        return std::make_unique<model::QuitCommand>();

      case 'h':
        return std::make_unique<model::GoParentCommand>();

      case 'l':
        return std::make_unique<model::GoCurrentCommand>();

      case 'j':
        return std::make_unique<model::CursorUpCommand>();

      case 'k':
        return std::make_unique<model::CursorDownCommand>();
      }
    }

    return std::make_unique<model::QuitAllCommand>();
  }
} // namespace controller
