#include "stream_provider.h"
#include "keyboard_provider.h"

namespace controller {
  bool StreamProvider::getCharPriv(KeyboardProvider::Char * const c) {
    assert(c);
    assert(stream_);

    char chr;
    if (stream_->get(chr)) {
      c->character_ = chr;
      c->shift_ = false;
      c->control_ = false;
      c->meta_ = false;
      c->hyper_ = false;
      c->super_ = false;
      return true;
    }

    return false;
  }
} // namespace controller
