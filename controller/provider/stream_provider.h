#pragma once
#ifndef CONTROLLER_PROVIDER_STREAM_PROVIDER_H
#define CONTROLLER_PROVIDER_STREAM_PROVIDER_H
#include <cassert>
#include <istream>

#include "keyboard_provider.h"

namespace controller {
  class StreamProvider final: public KeyboardProvider {
  public:
    explicit StreamProvider(std::istream* const stream): stream_(stream) {
      assert(stream_);
    }

    std::istream* stream() { return stream_; }
    const std::istream* stream() const { return stream_; }
    void stream(std::istream* const stream) {
      assert(stream);
      stream_ = stream;
    }

  private:
    bool getCharPriv(KeyboardProvider::Char * const) override;

    std::istream* stream_;
  };
} // namespace controller
#endif // CONTROLLER_PROVIDER_STREAM_PROVIDER_H
