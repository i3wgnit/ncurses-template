#pragma once
#ifndef CONTROLLER_PROVIDER_KEYBOARD_PROVIDER_H
#define CONTROLLER_PROVIDER_KEYBOARD_PROVIDER_H
#include <cassert>

namespace controller {
  class KeyboardProvider {
  public:
    virtual ~KeyboardProvider() = default;

    struct Char {
      int character_;
      bool shift_;
      bool control_;
      bool meta_;
      bool hyper_;
      bool super_;
    };

    bool getChar(Char * const c) {
      assert(c);
      return getCharPriv(c);
    }

  private:
    virtual bool getCharPriv(Char * const) = 0;
  };
} // namespace controller
#endif // CONTROLLER_PROVIDER_KEYBOARD_PROVIDER_H
