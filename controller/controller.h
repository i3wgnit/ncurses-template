#pragma once
#ifndef CONTROLLER_H
#define CONTROLLER_H
#include <memory>

#include "command/command.h"

namespace controller {
  class Controller {
  public:
    virtual ~Controller() = default;

    std::unique_ptr<model::Command> getCommand() {
      return getCommandPriv();
    }

  private:
    virtual std::unique_ptr<model::Command> getCommandPriv() = 0;
  };
} // namespace controller
#endif // CONTROLLER_H
