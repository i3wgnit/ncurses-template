#pragma once
#ifndef CONTROLLER_KEYBOARD_CONTROLLER_H
#define CONTROLLER_KEYBOARD_CONTROLLER_H
#include <memory>

#include "controller.h"

namespace controller {
  class KeyboardProvider;

  class KeyboardController final: public Controller {
  public:
    explicit KeyboardController(std::unique_ptr<KeyboardProvider> provider);

    std::unique_ptr<KeyboardProvider>& provider() { return provider_; }
    void provider(std::unique_ptr<KeyboardProvider>);

  private:
    std::unique_ptr<model::Command> getCommandPriv() override;

    std::unique_ptr<KeyboardProvider> provider_;
  };
} // namespace controller
#endif // CONTROLLER_KEYBOARD_CONTROLLER_H
