#include <algorithm>
#include <iostream>
#include <memory>

#include "prog.h"
#include "state/state.h"
#include "state/tab.h"
#include "state/window.h"

#include "stream_view.h"

#include "keyboard_controller.h"
#include "provider/stream_provider.h"

int main() {
  auto window = std::make_unique<model::Window>(
    model::Window::SortingMode::alpha, false, std::filesystem::current_path());
  auto tab = std::make_unique<model::Tab>(std::move(window));

  auto stream = std::make_unique<controller::StreamProvider>(&std::cin);

  model::Prog prog(
    model::State(std::move(tab)),
    std::make_unique<view::StreamView>(&std::cout),
    std::make_unique<controller::KeyboardController>(std::move(stream)));

  prog.run();

  return 0;
}
