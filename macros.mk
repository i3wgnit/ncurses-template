# variables
# DIR TDIR CUR_DIR
# comma empty space

# functions
# includes, -includes, includelibs, -includelibs
# add_srcs, add_incs
# pop_front


__twl_includes = $(foreach TDIR,$(1),\
	$(eval DIR += $(TDIR))\
	$(eval $(2) $(CUR_DIR)/module.mk)\
	$(foreach CALLBACK,$(3),$(call $(CALLBACK),$(CUR_DIR)))\
	$(call pop_front,DIR))

comma := ,
empty :=
space := $(empty) $(empty)

CUR_DIR = $(subst $(space),/,$(strip $(DIR)))


# $(call add_srcs,src1 src2 ...)
add_srcs = $(eval SRCS += $(addprefix $(CUR_DIR)/,$(1)))
# $(call add_incs,lib1 lib2 ...)
add_incs = $(eval INCS += $(addprefix -I,$(1)))
# $(call add_libs,lib1 lib2 ...)
add_libs = $(eval LIBS += $(addprefix -l,$(1)))

# $(call includes,source1 source2 ...[, callback1 callback2 ...])
includes = $(call __twl_includes,$(strip $(1)),include,$(2))
-includes = $(call __twl_includes,$(strip $(1)),-include,$(2))

# $(call includelibs,source1 source2 ...[, callback1 callback2 ...])
includelibs = $(call includes,$(1),add_incs $(2))
-includelibs = $(call -includes,$(1),add_incs $(2))

# $(call pop_front,var)
pop_front = $(eval $(1) := $(wordlist 2,$(words $($(1))),1st $($(1))))
