.POSIX:

EXEC := prog

include config.mk
-include config.mk.local

MODULES := model view controller
SRCS := main.cc

# =====================
# === Generic Rules ===
# =====================

include macros.mk

# $(call includes,module1 module2 ...)
# Includes new modules for make
#
# $(call includelibs,lib1 lib2 ...)
# Includes new modules for make and
# adds them as includes for compilation
#
# $(call add_srcs,src1 src2 ...)
# Adds new sources for compilation
#
# $(call add_incs,lib1 lib2 ...)
# Adds new includes for compilation
#
# $(call add_libs,lib1 lib2 ...)
# Adds new libraries for linking

release: $(EXEC)
	strip $(EXEC)
small: $(EXEC)
	strip $(EXEC)
debug: $(EXEC)

DIR :=
$(call includelibs,$(MODULES))

CXXFLAGS := $(CXXFLAGS_PRE) $(CXXFLAGS_RELEASE) $(INCS)
debug: CXXFLAGS := $(CXXFLAGS_PRE) $(CXXFLAGS_DEBUG) $(INCS)
small: CXXFLAGS := $(CXXFLAGS_PRE) $(CXXFLAGS_SMALL) $(INCS)

LDFLAGS := $(LDFLAGS_PRE) $(LDFLAGS_RELEASE) $(LIBS)
small: LDFLAGS := $(LDFLAGS_PRE) $(LDFLAGS_SMALL) $(LIBS)
debug: LDFLAGS := $(LDFLAGS_PRE) $(LDFLAGS_DEBUG) $(LIBS)

SRC := $(filter %.cc,$(SRCS))
OBJ := $(SRC:.cc=.o)
DEP := $(SRC:.cc=.d)

options:
	@echo build options:
	@echo "CXXFLAGS = $(CXXFLAGS)"
	@echo "LDFLAGS  = $(LDFLAGS)"
	@echo "CXX      = $(CXX)"

-include $(DEP)

%.o: %.cc
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(OBJ): config.mk config.mk.local

config.mk.local:

$(EXEC): options $(OBJ)
	$(CXX) -o $@ $(OBJ) $(LDFLAGS)

clean:
	rm -f $(EXEC) $(OBJ) $(DEP)

.PHONY: options release small debug clean
