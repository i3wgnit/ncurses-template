CXX := clang++

CXXFLAGS_PRE := -std=c++17 -stdlib=libstdc++ -Wall -MMD
CXXFLAGS_RELEASE := -DNDEBUG -O3
CXXFLAGS_SMALL := -DNDEBUG -Os
CXXFLAGS_DEBUG := -DDEBUG -g -fsanitize=address

LDFLAGS_PRE :=
LDFLAGS_RELEASE :=
LDFLAGS_SMALL :=
LDFLAGS_DEBUG := -fsanitize=address

INCS := -I.
LIBS := -lstdc++fs
