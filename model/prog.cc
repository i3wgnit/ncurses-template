#include "prog.h"
#include "view.h"
#include "controller.h"
#include "command/command.h"
#include "state/tab.h"
#include "state/window.h"

namespace model {
  Prog::Prog(): Model() {}

  void Prog::run() {
    do {
      updateViews(state_);
      std::unique_ptr<Command> command = getCommand();
      command->execute(&state_);
    } while (!state_.tabs().empty());
  }
} // namespace model
