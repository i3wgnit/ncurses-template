#pragma once
#ifndef MODEL_PROG_H
#define MODEL_PROG_H
#include "model.h"
#include "state/state.h"

namespace model {
  class Prog final: public Model {
  public:
    Prog();
    template<typename ...Args>
    explicit Prog(State state, Args... args):
      Model(std::forward<Args>(args)...),
      state_(std::move(state)) {}

    State& state() { return state_; }
    const State& state() const { return state_; }

    void run();

  private:
    State state_;
  };
} // namespace model
#endif // MODEL_PROG_H
