#pragma once
#ifndef MODEL_H
#define MODEL_H
#include <memory>
#include <vector>

namespace view {
  class View;
} // namespace view

namespace controller {
  class Controller;
} // namespace controller

namespace model {
  class Command;
  class State;

  class Model {
  public:
    using view_type = std::unique_ptr<view::View>;
    using controller_type = std::unique_ptr<controller::Controller>;

    Model() = default;
    Model(view_type, controller_type);
    virtual ~Model() = default;

    const std::vector<view_type>& views() const { return views_; }

    const std::vector<controller_type>& controllers() const {
      return controllers_;
    }

    void updateViews(const State&);
    std::unique_ptr<Command> getCommand();

  private:
    std::vector<view_type> views_;
    std::vector<controller_type> controllers_;
  };
} // namespace model
#endif // MODEL_H
