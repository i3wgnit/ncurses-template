#pragma once
#ifndef MODEL_STATE_WINDOW_H
#define MODEL_STATE_WINDOW_H
#include <deque>
#include <filesystem>

namespace model {
  class Window {
  public:
    using entries_type = std::deque<std::filesystem::directory_entry>;

    enum class SortingMode { noOrder, alpha, size, time, random };

    Window(): pwd_(std::filesystem::current_path()), entryIndex_(0) {
      reload();
    }

    explicit Window(std::filesystem::path pwd): pwd_(std::move(pwd)),
                                                entryIndex_(0) {
      reload();
    }

    Window(SortingMode sortingMode, bool reversed, std::filesystem::path pwd):
        sortingMode_(sortingMode), reversed_(reversed), pwd_(std::move(pwd)),
        entryIndex_(0) {
      reload();
      sort();
    }

    SortingMode sortingMode() const { return sortingMode_; }
    void sortingMode(SortingMode sortingMode) {
      if (sortingMode != sortingMode_) {
        sortingMode_ = sortingMode;
      }
    }

    bool reversed() const { return reversed_; }
    void reversed(bool reversed) { reversed_ = reversed; }

    const std::filesystem::path& pwd() const { return pwd_; }
    void pwd(std::filesystem::path pwd) {
      pwd_ = std::move(pwd);
    }

    const entries_type& directories() const { return directories_; }

    const entries_type& files() const { return files_; }

    size_t entryIndex() const { return entryIndex_; }
    void entryIndex(size_t entryIndex) { entryIndex_ = entryIndex; }
    void entryIndex(const std::string& filename, const bool fileIsDirectory);

    bool empty() const { return directories_.empty() && files_.empty(); }

    size_t size() const { return directories_.size() + files_.size(); }

    bool entryIsFile() const {
      return entryIndex_ >= directories_.size() && !files_.empty();
    }

    bool entryIsDirectory() const { return entryIndex_ < directories_.size(); }

    void refresh();

    void reload();

    void sort();

  private:
    SortingMode sortingMode_;
    bool reversed_;
    std::filesystem::path pwd_;
    entries_type directories_;
    entries_type files_;
    size_t entryIndex_;
  };
} // namespace model
#endif // MODEL_STATE_WINDOW_H
