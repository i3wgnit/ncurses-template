#include <algorithm>
#include <cassert>
#include <iterator>
#include <random>

#include "window.h"
#include "macros.h"

namespace fs = std::filesystem;

namespace model {
  void Window::entryIndex(const std::string& filename,
                          const bool fileIsDirectory) {
    entryIndex_ = 0;
    auto isFile =
      [&filename](const fs::directory_entry& e) -> bool {
        return filename == e.path().filename().string();
      };
    if (fileIsDirectory) {
      auto it = std::find_if(directories_.cbegin(), directories_.cend(),
                             isFile);
      if (it != directories_.cend()) {
        entryIndex_ = std::distance(directories_.cbegin(), std::move(it));
      }
    } else {
      auto it = std::find_if(files_.cbegin(), files_.cend(), isFile);
      if (it != files_.cend()) {
        entryIndex_ = directories_.size() +
          std::distance(files_.cbegin(), std::move(it));
      }
    }
  }

  void Window::refresh() {
    if (empty()) {
#ifdef IS_DEBUG
      entryIndex_ = 0;
#endif
      reload();
      sort();
    } else {
      assert(entryIndex_ < size());

      std::string filename;
      const bool fileIsDirectory = entryIsDirectory();
      if (fileIsDirectory) {
        filename = directories_[entryIndex_].path().filename().string();
      } else {
        filename = files_[entryIndex_ - directories_.size()]
          .path().filename().string();
      }

      reload();
      sort();

      entryIndex(filename, fileIsDirectory);
    }
  }

  void Window::reload() {
    assert(fs::exists(pwd_));

    directories_.clear();
    files_.clear();
    for (fs::directory_entry entry : fs::directory_iterator(pwd_)) {
      if (entry.is_directory()) {
        directories_.emplace_back(std::move(entry));
      } else {
        files_.emplace_back(std::move(entry));
      }
    }
  }

  void Window::sort() {
    auto alphaCmp =
      [](const auto& a, const auto& b) {
        assert(a.exists());
        assert(b.exists());
        return a < b;
      };
    auto sizeCmp =
      [](const auto& a, const auto& b) {
        assert(a.exists());
        assert(b.exists());
        return a.file_size() < b.file_size();
      };
    auto timeCmp =
      [](const auto& a, const auto& b) {
        assert(a.exists());
        assert(b.exists());
        return a.last_write_time() < b.last_write_time();
      };

    std::random_device rd;
    std::mt19937 g(rd());

    switch (sortingMode_) {
    case SortingMode::alpha:
      std::sort(directories_.begin(), directories_.end(), alphaCmp);
      std::sort(files_.begin(), files_.end(), alphaCmp);
      break;

    case SortingMode::size:
      std::sort(directories_.begin(), directories_.end(), alphaCmp);
      std::sort(files_.begin(), files_.end(), sizeCmp);
      break;

    case SortingMode::time:
      std::sort(directories_.begin(), directories_.end(), timeCmp);
      std::sort(files_.begin(), files_.end(), timeCmp);
      break;

    case SortingMode::noOrder:
      std::shuffle(directories_.begin(), directories_.end(), g);
      std::shuffle(files_.begin(), files_.end(), g);
      break;

    default:
      return;
    }
  }
} // namespace model
