#include "tab.h"
#include "window.h"

namespace model {
  Tab::Tab(): currentWindow_(0) {}
  Tab::Tab(window_type window): currentWindow_(0) {
    windows_.emplace_back(std::move(window));
  }
} // namespace model
