#pragma once
#ifndef MODEL_STATE_STATE_INCLUDE_H
#define MODEL_STATE_STATE_INCLUDE_H
#include <cassert>
#include "state.h"
#include "tab.h"
#include "window.h"

#define __MODEL_STATE_SETUP()                               \
  assert(currentTab < tabs.size());                         \
  auto& tab = tabs[currentTab];                             \
  assert(tab);                                              \
  auto& windows = tab->windows();                           \
  assert(!windows.empty());                                 \
  const size_t currentWindow = tab->currentWindow();        \
  assert(currentWindow < windows.size());                   \
  [[maybe_unused]] auto& window = windows[currentWindow];   \
  assert(window)

#define MODEL_STATE_SETUP_STAR()                    \
  assert(state);                                    \
  auto& tabs = state->tabs();                       \
  assert(!tabs.empty());                            \
  const size_t currentTab = state->currentTab();    \
  __MODEL_STATE_SETUP()

#define MODEL_STATE_SETUP_REF()                 \
  auto& tabs = state.tabs();                    \
  assert(!tabs.empty());                        \
  const size_t currentTab = state.currentTab(); \
  __MODEL_STATE_SETUP()
#endif // MODEL_STATE_STATE_INCLUDE_H
