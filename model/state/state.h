#pragma once
#ifndef MODEL_STATE_H
#define MODEL_STATE_H
#include <memory>
#include <vector>

namespace model {
  class Tab;

  class State final {
  public:
    using tab_type = std::unique_ptr<Tab>;

    State();
    explicit State(tab_type tab);

    size_t currentTab() const { return currentTab_; }
    void currentTab(size_t tab) { currentTab_ = tab; }

    std::vector<tab_type>& tabs() { return tabs_; }
    const std::vector<tab_type>& tabs() const { return tabs_; }

  private:
    size_t currentTab_;
    std::vector<tab_type> tabs_;
  };
} // namespace model
#endif // MODEL_STATE_H
