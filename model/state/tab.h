#pragma once
#ifndef MODEL_STATE_TAB_H
#define MODEL_STATE_TAB_H
#include <memory>
#include <vector>

namespace model {
  class Window;

  class Tab final {
  public:
    using window_type = std::unique_ptr<Window>;

    Tab();
    explicit Tab(window_type window);

    size_t currentWindow() const { return currentWindow_; }
    void currentWindow(size_t window) { currentWindow_ = window; }

    std::vector<window_type>& windows() { return windows_; }
    const std::vector<window_type>& windows() const { return windows_; }

  private:
    size_t currentWindow_;
    std::vector<window_type> windows_;
  };
} // namespace model
#endif // MODEL_STATE_TAB_H
