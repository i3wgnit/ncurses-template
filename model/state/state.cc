#include "state.h"
#include "tab.h"
#include "window.h"

namespace model {
  State::State(): currentTab_(0) {}
  State::State(tab_type tab): currentTab_(0) {
    tabs_.emplace_back(std::move(tab));
  }
} // namespace model
