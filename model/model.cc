#include <cassert>

#include "model.h"
#include "view.h"
#include "controller.h"

namespace model {
  Model::Model(view_type view, controller_type controller):
    views_(1), controllers_(1) {
    assert(view);

    views_[0].swap(view);
    assert(controller);
    controllers_[0].swap(controller);
  }

  void Model::updateViews(const State& state) {
    for (view_type& view: views_) {
      assert(view);
      view->update(state);
    }
  }

  std::unique_ptr<Command> Model::getCommand() {
    assert(!controllers_.empty());
    return controllers_.front()->getCommand();
  }
} // namespace model
