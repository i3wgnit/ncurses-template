#include "cursor_up.h"
#include "state/state_include.h"

namespace model {
  void CursorUpCommand::executePriv(State* const state) {
    MODEL_STATE_SETUP_STAR();

    const size_t next = window->entryIndex() + 1;
    if (next < window->size()) {
      window->entryIndex(next);
    } else if (!window->empty()) {
      window->entryIndex(0);
    }
  }
} // namespace model
