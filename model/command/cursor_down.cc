#include "cursor_down.h"
#include "state/state_include.h"

namespace model {
  void CursorDownCommand::executePriv(State* const state) {
    MODEL_STATE_SETUP_STAR();

    const size_t index = window->entryIndex();
    if (index > 0) {
      window->entryIndex(index - 1);
    } else if (!window->empty()) {
      window->entryIndex(window->size() - 1);
    }
  }
} // namespace model
