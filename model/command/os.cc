#include "os.h"

namespace model {
  std::string popen_(const std::string& command) {
    std::array<char, 128> buffer;
    FILE *pipe = popen(command.c_str(), "r");
    if (!pipe) {
      std::perror("Popen failed.");
      return "";
    }
    if (!std::fgets(buffer.data(), 128, pipe)) {
      std::perror("Fgets failed.");
      return "";
    }
    pclose(pipe);
    return buffer.data();
  }

  void forkExec_(const std::vector<std::string>& command) {
    if (command.empty()) {
      return;
    }

    pid_t pid = fork();

    if (pid == -1) {
      std::perror("Fork failed.");
      return;
    }

    std::vector<char*> args;
    args.reserve(command.size() + 1);
    for (const std::string& arg : command) {
      args.emplace_back(const_cast<std::string&>(arg).data());
    }
    args.emplace_back(nullptr);

    if (!pid) {
      execvp(args[0], args.data());
      std::perror("Execve failed.");
      std::exit(EXIT_FAILURE);
    } else {
      int status;
      pid_t w = waitpid(pid, &status, WUNTRACED | WCONTINUED);
      if (w == -1) {
        std::perror("Waitpid failed.");
      }
    }
  }
} // namespace model
