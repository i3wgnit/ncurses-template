#include <filesystem>

#include "go_parent.h"
#include "state/state_include.h"

namespace fs = std::filesystem;

namespace model {
  void GoParentCommand::executePriv(State* const state) {
    MODEL_STATE_SETUP_STAR();

    const fs::path& pwd = window->pwd();
    if (pwd.has_relative_path()) {
      std::string filename = pwd.filename().string();
      window->pwd(pwd.parent_path());
      window->reload();
      window->sort();
      window->entryIndex(filename, true);
    }
  }
} // namespace model
