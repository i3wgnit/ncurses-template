#pragma once
#ifndef MODEL_COMMAND_CURSOR_UP_H
#define MODEL_COMMAND_CURSOR_UP_H
#include "command.h"

namespace model {
  class CursorUpCommand final: public Command {
  private:
    void executePriv(State* const) override;
  };
} // namespace model
#endif // MODEL_COMMAND_CURSOR_UP_H
