#pragma once
#ifndef MODEL_COMMAND_QUIT_ALL_H
#define MODEL_COMMAND_QUIT_ALL_H
#include "command.h"

namespace model {
  class QuitAllCommand final: public Command {
  private:
    void executePriv(State* const state) override;
  };
} // namespace model
#endif // MODEL_COMMAND_QUIT_ALL_H
