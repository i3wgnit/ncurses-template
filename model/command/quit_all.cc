#include "quit_all.h"
#include "state/state_include.h"

namespace model {
  void QuitAllCommand::executePriv(State* const state) {
    MODEL_STATE_SETUP_STAR();

    tabs.clear();
  }
} // namespace model
