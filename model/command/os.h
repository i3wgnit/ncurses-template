#pragma once
#ifndef MODEL_COMMAND_OS_H
#define MODEL_COMMAND_OS_H
#include <array>
#include <cstdlib>
#include <string>
#include <vector>

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

namespace model {
  std::string popen_(const std::string&);
  void forkExec_(const std::vector<std::string>&);
} // namespace model
#endif // MODEL_COMMAND_OS_H
