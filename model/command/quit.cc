#include "quit.h"
#include "state/state_include.h"

namespace model {
  void QuitCommand::executePriv(State* const state) {
    MODEL_STATE_SETUP_STAR();

    if (windows.size() == 1) {
#ifdef IS_DEBUG
      if (tabs.size() == 1) {
        state->currentTab(0);
      }
#endif
      tabs.erase(tabs.cbegin() + currentTab);
      if (!tabs.empty()) {
        if (currentTab == tabs.size()) {
          state->currentTab(0);
        } else {
          state->currentTab(currentTab);
        }
      }
    } else {
      windows.erase(windows.cbegin() + currentWindow);
      if (currentWindow == windows.size()) {
        tab->currentWindow(0);
      } else {
        tab->currentWindow(currentWindow);
      }
    }
  }
} // namespace model
