#pragma once
#ifndef MODEL_COMMAND_CURSOR_DOWN_H
#define MODEL_COMMAND_CURSOR_DOWN_H
#include "command.h"

namespace model {
  class CursorDownCommand final: public Command {
  private:
    void executePriv(State* const) override;
  };
} // namespace model
#endif // MODEL_COMMAND_CURSOR_DOWN_H
