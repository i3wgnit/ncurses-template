#pragma once
#ifndef MODEL_COMMAND_GO_PARENT_H
#define MODEL_COMMAND_GO_PARENT_H
#include "command.h"

namespace model {
  class GoParentCommand final: public Command {
  private:
    void executePriv(State* const) override;
  };
} // namespace model
#endif // MODEL_COMMAND_GO_PARENT_H
