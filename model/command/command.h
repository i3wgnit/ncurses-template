#pragma once
#ifndef MODEL_COMMAND_H
#define MODEL_COMMAND_H

namespace model {
  class State;

  class Command {
  public:
    virtual ~Command() = default;

    void execute(State* const state) {
      executePriv(state);
    }

  private:
    virtual void executePriv(State* const) = 0;
  };
} // namespace model
#endif // MODEL_COMMAND_H
