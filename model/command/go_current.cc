#include <filesystem>
#include <regex>
#include <string>
#include <vector>

#include "go_current.h"
#include "os.h"
#include "state/state_include.h"

namespace fs = std::filesystem;

namespace {
  std::vector<std::string> getOpenCommand_(const fs::path& file) {
    std::string command ("file -b --mime-type -- '");
    command.append(file.native());
    command += '\'';
    const std::string mimetype = model::popen_(command);
    const std::string filename = file.filename();
    const std::string extension = file.extension();

    std::vector<std::string> ret;

    std::regex re ("\\.x?html?");

    if (std::regex_match(extension, re)) {
#ifdef __APPLE__
      ret.emplace_back("open");
      ret.emplace_back("-a");
#endif
      ret.emplace_back("firefox");
      goto end;
    }

    re = "^text";
    if (std::regex_search(mimetype, re)) {
      ret.emplace_back("vim");
      goto end;
    }

#ifdef __APPLE__
    ret.emplace_back("open");
#else
    return ret;
#endif

  end:
    ret.emplace_back("--");
    ret.emplace_back(file.native());
    return ret;
  }
} // namespace

namespace model {
  void GoCurrentCommand::executePriv(State* const state) {
    MODEL_STATE_SETUP_STAR();

    if (window->empty()) {
      return;
    }

    const size_t index = window->entryIndex();

    if (window->entryIsFile()) {
      const size_t fileIndex = index - window->directories().size();
      const fs::directory_entry& entry = window->files()[fileIndex];
      fs::current_path(window->pwd());
      forkExec_(getOpenCommand_(entry.path()));
    } else if (window->entryIsDirectory()) {
      const fs::directory_entry& entry = window->directories()[index];
      window->pwd(entry.path());
      window->entryIndex(0);
      window->reload();
      window->sort();
    }
  }
} // namespace model
