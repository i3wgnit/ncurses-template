#pragma once
#ifndef MODEL_COMMAND_GO_CURRENT_H
#define MODEL_COMMAND_GO_CURRENT_H
#include "command.h"

namespace model {
  class GoCurrentCommand final: public Command {
  private:
    void executePriv(State* const) override;
  };
} // namespace model
#endif // MODEL_COMMAND_GO_CURRENT_H
